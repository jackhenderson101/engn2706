function SIM = bm25_all_docs(WD, k, b)
%% calculates the bm25 document similarity between two documents
if (nargin == 1)
	k=8.0;
	b=0.7;
end

% n = number of documents total
n = size(WD,2);

ndocs = sum(WD~=0,2);

% dl = wordcount of each document
dl = sum(WD,1);
avgdl = mean(dl);

SIM = zeros(n,n);
idf = log2(n./ndocs);
idf(isinf(idf)) = 0;

tic
for doc1 = 1:10%size(WD,2)
	bl_i = (1-b)+b*dl(doc1)/avgdl;

	fprintf('doc1=%d\n',doc1);

	for doc2 = 1:size(WD,2)
	    if(doc1<=doc2)
	        continue
	    end
	    %fprintf('doc %d\n', doc2);
        res = bm25(doc1,doc2,WD,k,b, dl, avgdl, idf, bl_i);
	   SIM(doc1, doc2) = res;
       SIM(doc2, doc1) = res;
	end
end
toc