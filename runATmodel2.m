[WS, DS] = importworddoccounts('data/wordcount');
AD = importauthordoccounts('data/authorList');

AD = fixAuthors(AD);

% Set the number of topics
T = 20; 

%%
% Set the hyperparameters
BETA  = 0.01;
ALPHA = 50/T;

%%
% The number of iterations
BURNIN   = 20; % the number of iterations before taking samples
LAG      = 10; % the lag between samples
NSAMPLES = 1; % the number of samples for each chain
NCHAINS  = 1; % the number of chains to run 

%%
% What output to show (0=no output; 1=iterations; 2=all output)
OUTPUT = 2;

%% 
% The starting seed number
SEED = 9;


fprintf( 'Starting the Sampler\n' );

for c=1:NCHAINS
    SEED = SEED + 1; 
    N = BURNIN;  
    fprintf( 'Running Gibbs sampler for burnin\n' );
    [ WP, AT , Z , X ] = GibbsSamplerAT( WS , DS , AD , T , N , ALPHA , BETA , SEED , OUTPUT );
        
    fprintf( 'Continue to run sampler to collect samples\n' );
    for s=1:NSAMPLES
        docTopics = zeros(size(AD,2), T);
        for i = 1:size(DS,2)
           docTopics(DS(i),Z(i)) = docTopics(DS(i),Z(i)) +1;
        end
        
        filename = sprintf( 'out/at_chain%d_sample%d.mat' , c , s );
        fprintf( 'Saving sample #%d from chain #%d: filename=%s\n' , s , c , filename );
        comm = sprintf( 'save ''%s'' WP AT Z X ALPHA BETA SEED N T s c docTopics' , filename );
        eval( comm );
        

        
        if (s < NSAMPLES)
           N = LAG;
           SEED = SEED + 1; % important -- change the seed between samples !!
           [ WP, AT , Z , X ] = GibbsSamplerAT( WS , DS , AD , T , N , ALPHA , BETA , SEED , OUTPUT , Z , X );
        end
    end
end

saveTopics;