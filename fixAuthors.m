function AD =  fixAuthors(AD)
%fprintf('Fixing missing authors:\n');
% 


[ exist,dup ] = textread('import/duplicates.txt' , '%d %d');
for i = 1:size(exist,1) 
    AD(:,dup(i)) = AD(:,exist(i));
end

AD = double(AD > 0);
missing = find(sum(AD,1) ==0);
authors = size(AD,1);

for i = 1:size(missing,2)
	fprintf('%d\n', missing(i))
    AD(i+authors,missing(i))=1;
end
fprintf('Allocated %d new authors\n', size(missing,2));
