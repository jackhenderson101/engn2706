function RANK = getBM25rankingDoc(DOCUMENT, AD, WD)
%returns a ranking of all the document indices based on their similarity to
%the AUTHOR specified. The first document index is the most similar



%docSim = bm25_all_docs(WD);



avgRankings = csvread('out/avg_bm25.csv')';

bmScores = bm25_single(WD,DOCUMENT);

% take the subset of bmScores for which we have authorship info.
bmScores = bmScores(1:3563);
authorSim = zeros(1,size(AD,1));

for i = 1:size(AD,1)
    AUTHOR = i;
    docs = AD(AUTHOR,:);
    
    scores = (docs .* bmScores) ./ avgRankings;
    authorScore = mean(scores(scores>0));
    authorSim(i) = authorScore;
    
end
authorSim = authorSim';
%add document indicies to second row
authorSim(:,2) = 1:size(authorSim,1);
%sort by document distance
sorted = sortrows(authorSim,1);
%flip the result as BM25 measured similarity (bigger value is better)
sorted = flipud(sorted);
RANK = sorted(:,2);

%this is used for the scatter plots
%add document rankings to third row
%sorted(:,3) = 1:size(avgDists,1);
%sorted = sortrows(sorted,2);
%RANK = sorted(:,3);

end