function authorDocumentRankings()
%%Generate the list of candiate documents for each author, ranked in order
%%from most similar to least
AUTHORS = [17, 20, 32, 39, 44, 81, 161, 184, 185, 230, 242, 247, 257, 266, 318, 322, 358, 381, 387, 394, 420];
AD = importauthordoccounts('data/authorList');
AD = fixAuthors(AD);
bmRank = zeros(size(AD,2), size(AUTHORS,2));
atRank = zeros(size(AD,2), size(AUTHORS,2));

parfor idx = 1:size(AUTHORS,2)
    AUTHOR = AUTHORS(idx);
    fprintf('Generating Data for Author %d\n', AUTHOR);
    bmRank(:,idx) = getBM25ranking(AUTHOR);
    atRank(:,idx) = getATranking(AUTHOR);
    
    
    
end

csvwrite('out/atRank_selected_authors.csv', atRank);
csvwrite('out/bmRank_selected_authors.csv', bmRank);
end
