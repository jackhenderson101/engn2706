function docs =  authorDocument20Ranks()
ranks(1,:,:) = csvread('out/atRank_selected_authors.csv');
ranks(2,:,:) = csvread('out/bmRank_selected_authors.csv');
AD = importauthordoccounts('data/authorList');
AD = fixAuthors(AD);
AUTHORS = [17, 20, 32, 39, 44, 81, 161, 184, 185, 230, 242, 247, 257, 266, 318, 322, 358, 381, 387, 394, 420];
PERCENTILES = [5,10,20,40,80];


% get rank numbers corresponding to the percentiles
PERCENTILES = floor(PERCENTILES .* 0.01 .* size(AD,2));

for alg = 1:size(ranks,1)
    for idx = 1:size(AUTHORS,2)
        AUTHOR = AUTHORS(idx);
        i = 1;
        found = 0;
        %get top 5 ranked docs
        while found < 5
            %if document is not authored by AUTHOR
            if AD(AUTHOR,ranks(alg,i, idx)) == 0
                docs(found+1+(alg-1)*10, idx) = ranks(alg,i, idx);
                found = found +1;
            end
            i = i+1;
        end
        i=found + 1;
        for p = PERCENTILES
            if AD(AUTHOR,ranks(alg,p, idx)) == 1
                fprintf('This is a minor problem');
            end
            docs(i+(alg-1)*10, idx) = ranks(alg,p, idx);
            i = i+1;
        end
        
        
    end
end



