function RANK  = getATrankingDoc( DOCUMENT, DP_all )
%returns a ranking of all the document indices based on their similarity to
%the AUTHOR specified. The first document index is the most similar
%%%%
%%%% Not fully implemented yet.
%%%%

SEEDS = [14,16,18,19];
numAuthors = 433;
dists = zeros(size(SEEDS,2), numAuthors);

for j = 1:size(SEEDS,2)
    filename = sprintf('out/at_chain_50_topics_seed%d.mat', SEEDS(j));
    load(filename);
    fprintf('Generating DocDist for Document %d with seed %d\n', DOCUMENT, SEED)
    
    DP = squeeze(DP_all(j, :, :));
    
    %D = size( DP , 1 )
    T = size( DP , 2 );
    %authors = size(AT,1);
    
    sumdp = full( sum( DP , 2 )) + ALPHA * T;
    sumat = full( sum( AT , 2 )) + ALPHA * T;
    
    authordist = zeros( 1 , numAuthors );
    i1=DOCUMENT;
    dp1 = ( DP( i1, : ) + ALPHA ) / sumdp( i1 );
    
    for i2=1:numAuthors
        dp2 = ( AT( i2 , : ) + ALPHA ) / sumat( i2 );
        
        % calculate KL distances both ways
        KL1 = sum( dp1 .* log2( dp1 ./ dp2 ));
        KL2 = sum( dp2 .* log2( dp2 ./ dp1 ));
        
        authordist(i2 ) = (KL1+KL2)/2;
        %docdist( i2,i1 ) = (KL1+KL2)/2;
    end
    dists(j,:) = authordist; %generateDocDist(DOCUMENT, DS, filename);
end

avgDists = mean(dists)';

%add document indicies to second row
avgDists(:,2) = 1:size(avgDists,1);
%sort by document distance
sorted = sortrows(avgDists,1);

RANK = sorted(:,2);
%this is used for the scatter plots
%add document rankings to third row
%sorted(:,3) = 1:size(avgDists,1);
%sorted = sortrows(sorted,2);
%RANK = sorted(:,3);

end

