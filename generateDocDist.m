function docdist = generateDocDist(AUTHOR, DS, filename)
% Calculate the similarity between an author and every document in the
% corpus, given an AT model dataset specified by the filename

%load the at model data. Contains T,Z,ALPHA,docTopics, etc.
load(filename);
fprintf('Generating DocDist for Author %d with seed %d\n', AUTHOR, SEED)

numDocs = size(docTopics,1);
DP = zeros(numDocs, T);


for i = 1:size(DS,2)
   DP(DS(i),Z(i)) = DP(DS(i),Z(i)) +1;
end

%D = size( DP , 1 )
T = size( DP , 2 );
%authors = size(AT,1);

sumdp = full( sum( DP , 2 )) + ALPHA * T;
sumat = full( sum( AT , 2 )) + ALPHA * T;

docdist = zeros( 1 , numDocs );
i1=AUTHOR;
dp1 = ( AT( i1, : ) + ALPHA ) / sumat( i1 );

for i2=1:numDocs
    dp2 = ( DP( i2 , : ) + ALPHA ) / sumdp( i2 );

    % calculate KL distances both ways       
    KL1 = sum( dp1 .* log2( dp1 ./ dp2 ));    
    KL2 = sum( dp2 .* log2( dp2 ./ dp1 )); 

    docdist(i2 ) = (KL1+KL2)/2;
    %docdist( i2,i1 ) = (KL1+KL2)/2;
end
