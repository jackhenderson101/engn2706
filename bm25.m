function OK = bm25(doc1, doc2, WD, k, b, dl, avgdl, idf, bl_i)
%% calculates the bm25 document similarity between two documents


bl_j = (1-b)+b*dl(doc2)/avgdl;
WD_doc1 = WD(:,doc1);
WD_doc2 = WD(:, doc2);

%for term = 1:size(WD,1);
%
%	tf_il = WD_doc1(term);
%	tf_jl = WD_doc2(term);
%	
%	OK = OK + ((tf_jl * (k+1)) / (tf_jl + k*bl_j)) * ((tf_il * (k+1)) / (tf_il + k*bl_i)) * idf(term);
%
%end;

%fprintf("Loop: %d \n", OK)
OK = sum( ((WD_doc2 * (k+1)) ./ (WD_doc2 + k*bl_j)) .* ((WD_doc1 * (k+1)) ./ (WD_doc1 + k*bl_i)) .* idf );
%fprintf("Vec: %d \n", OK)
