function AD  = importauthordoccounts( filename , nheaderlines);
%% Imports text file with author-document counts into matlab format
%
%%
%
%%
% NOTES
% The text file should be organized into
% two colums where each row contains the author index, the document index.
% For example:
%      1 2
%      1 3
%      2 2

if nargin==1
    nheaderlines = 0;
end

fprintf( 'Importing author document counts from: %s\n' , filename );

[ aa,dd ] = textread( filename , '%d %d' , 'headerlines' , nheaderlines );
AD = sparse( aa,dd,1);

fprintf( 'Number of Documents D = %d\n' , size( AD , 2 ));
fprintf( 'Number of Authors   A = %d\n' , size( AD , 1 ));
fprintf( 'Number of nonzero entries in matrix NNZ=%d\n' , nnz( AD ));

%AD  = SparseMatrixtoCounts( AD );

