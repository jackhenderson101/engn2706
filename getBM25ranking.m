function RANK = getBM25ranking(AUTHOR)
%returns a ranking of all the document indices based on their similarity to
%the AUTHOR specified. The first document index is the most similar

WD = importWordDocFreq('data/wordcount');

%docSim = bm25_all_docs(WD);

AD = importauthordoccounts('data/authorList');
AD = fixAuthors(AD);

avgRankings = csvread('out/avg_bm25.csv')';

authorDocs = find(AD(AUTHOR,:));
SIM = zeros(size(authorDocs,2),size(WD,2));
for i = 1:size(authorDocs,2)
    doc = authorDocs(i);
    fprintf('Comparing doc %d\n', doc);
    SIM(i,:) = bm25_single(WD,doc);
end

avgDists = mean(SIM) ./ avgRankings;
avgDists = avgDists';

%add document indicies to second row
avgDists(:,2) = 1:size(avgDists,1);
%sort by document distance
sorted = sortrows(avgDists,1);
%flip the result as BM25 measured similarity (bigger value is better)
sorted = flipud(sorted);
RANK = sorted(:,2);

%this is used for the scatter plots
%add document rankings to third row
%sorted(:,3) = 1:size(avgDists,1);
%sorted = sortrows(sorted,2);
%RANK = sorted(:,3);

end