function DP = getDP()
SEEDS = [14,16,18,19];
[~, DS] = importworddoccounts('data/wordcount');

AD = importauthordoccounts('data/authorList');
AD = fixAuthors(AD);
DP = zeros(size(SEEDS,2),size(AD,2), 50);

for j = 1:size(SEEDS,2)
    filename = sprintf('out/at_chain_50_topics_seed%d.mat', SEEDS(j));
    load(filename);
    fprintf('Generating DP for seed %d\n', SEED)
    
    for i = 1:size(DS,2)
        DP(j,DS(i),Z(i)) = DP(j,DS(i),Z(i)) +1;
    end
end
end