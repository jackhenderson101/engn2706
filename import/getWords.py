from sys import argv
from sets import Set

docIds = Set([])
badWords = Set([])
stopwords = Set(['i', 'me', 'my', 'myself', 'we', 'our', 'ours', 'ourselves', 'you', 'your', 'yours', 'yourself', 'yourselves', 'he', 'him', 'his', 'himself', 'she', 'her', 'hers', 'herself', 'it', 'its', 'itself', 'they', 'them', 'their', 'theirs', 'themselves', 'what', 'which', 'who', 'whom', 'this', 'that', 'these', 'those', 'am', 'is', 'are', 'was', 'were', 'be', 'been', 'being', 'have', 'has', 'had', 'having', 'do', 'does', 'did', 'doing', 'would', 'should', 'could', 'ought', 'a', 'an', 'the', 'and', 'but', 'if', 'or', 'because', 'as', 'until', 'while', 'of', 'at', 'by', 'for', 'with', 'about', 'against', 'between', 'into', 'through', 'during', 'before', 'after', 'above', 'below', 'to', 'from', 'up', 'down', 'in', 'out', 'on', 'off', 'over', 'under', 'again', 'further', 'then', 'once', 'here', 'there', 'when', 'where', 'why', 'how', 'all', 'any', 'both', 'each', 'few', 'more', 'most', 'other', 'some', 'such', 'no', 'nor', 'not', 'only', 'own', 'same', 'so', 'than', 'too', 'very'])
script, input, wordlist, wordcount, errors = argv
f = open(input)
wlist = open(wordlist, 'w')
wcount = open(wordcount, 'w')
errorfile = open(errors, 'w')
i=0
for line in f:
  words = line.split(', ')
  if(words[0] in stopwords):
    print "removing stopword: "+words[0]
    continue
  
  i+=1
  wlist.write(words[0]+'\n')
  for word in words[2:]:
    pair = word.strip()[1:-1].split()
    if(len(pair) != 2):
      badWords.add(words[0])
      continue
    docId, freq = pair
    if(int(docId) > 3563):
      continue
    docIds.add(docId)
    wcount.write(docId+' '+str(i)+' '+freq+'\n')
errorfile.write(str(sorted(badWords)))
print "number of documents: "+str(len(docIds))
print "words with errors: "+str(len(badWords))
