from sys import argv
from sets import Set
import re
docIds = Set([])
authorCount = [0] * 4223
duplicates = Set([])

script, authorship, output, errors, dups = argv
f = open(authorship)
out = open(output, 'w')
errorfile = open(errors, 'w')
dupsfile = open(dups, 'w')

#num_authors = sum(1 for line in f)
#f.seek(0)
#num_docs = len(set(re.split('\W+',f.read()[:-1])))
#f.seek(0)
#print re.split('\W+',f.read()[:-1])
#print "Authors: " + str(num_authors)
#print "Documents: " + str(num_docs)
#num_docs = 4223
#matrix = [[0 for x in range(num_docs+1)] for x in range(num_authors+1)]

i = 0
for line in f:
  i +=1
  docs = re.split('\W+', line)[:-1]
  if(len(set(docs)) != len(docs)):
    duplicates.add(i)
  for doc in docs:
    docIds.add(int(doc))
    authorCount[int(doc)] =authorCount[int(doc)]+1 
    out.write(str(i) + ' '+doc+'\n')
print "number of documents: "+str(len(docIds))
full = set(range(1,4223))
errorfile.write(str(sorted(full.difference(docIds))))
dupsfile.write(str(sorted(duplicates)))
print "number of docs missing authors: " +str(len(full.difference(docIds)))
print "number of authors with duplicate documents: "+str(len(duplicates))
singleAuths = len(filter((lambda x: x==1),authorCount))
print "number of documents with single authors: "+str(singleAuths)+", "+str((singleAuths*100)/4223)+"%"