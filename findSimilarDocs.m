function DOCS = findSimilarDocs(AUTHOR)
% Returns a list of documents from each model that are the ranked as the most similar to a give author

[~, DS] = importworddoccounts('data/wordcount');

SEEDS = [14,16,18,19];

generateDocDist(AUTHOR, DS)
end