
DP = docTopics;
D = size( DP , 1 );
T = size( DP , 2 );

sumdp = full( sum( DP , 2 )) + ALPHA * T;

docdist = zeros( D , D );
for i1=1:D
    fprintf('Doc %d\n', i1);
    dp1 = ( DP( i1, : ) + ALPHA ) / sumdp( i1 );
    
    for i2=i1+1:D
        dp2 = ( DP( i2 , : ) + ALPHA ) / sumdp( i2 );
        
        % calculate KL distances both ways       
        KL1 = sum( dp1 .* log2( dp1 ./ dp2 ));    
        KL2 = sum( dp2 .* log2( dp2 ./ dp1 )); 
        
        docdist( i1,i2 ) = (KL1+KL2)/2;
        docdist( i2,i1 ) = (KL1+KL2)/2;
    end
end