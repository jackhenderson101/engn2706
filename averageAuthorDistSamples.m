[WS, DS] = importworddoccounts('data/wordcount');


SEEDS = [14,16,18,19];
AUTHOR = 1;
dists = zeros( 4, 3563);
for j = 1:size(SEEDS,2)
   filename = sprintf('out/at_chain_50_topics_seed%d.mat', SEEDS(j));
   load(filename);
   generateDocDist;
   t = docdist(AUTHOR,:);
   dists(j,:) = t; 
end
    