
ranks(1,:,:) = csvread('out/atRank_selected_authors.csv');
ranks(2,:,:) = csvread('out/bmRank_selected_authors.csv');
maxranks = zeros(size(ranks,1),size(ranks,2));
avgranks = zeros(size(ranks,1),size(ranks,2));


for alg = 1:size(ranks,1)
    rank = squeeze(ranks(alg,:,:));
    for x = 1:size(rank,1)
        [r,c] = find(rank == x);
        maxranks(alg,x) = max(r);
        avgranks(alg,x) = mean(r);
    end
    
end