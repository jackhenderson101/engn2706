function WD = importWordDocFreq(filename);
%%imports the file into a W X D+1 matrix, where W is the number of terms and D is the numeber of documents.
% WD[m,n] is the frequency of word m in document n. the last column of WD is the corpus frequency of each term.

fprintf( 'Importing word document counts from: %s\n' , filename );
[ dd,ww,cc ] = textread( filename , '%d %d %d');
WD = full(sparse( ww,dd,cc ));

fprintf( 'Number of Documents D = %d\n' , size( WD , 2 ));
fprintf( 'Number of Words     W = %d\n' , size( WD , 1 ));