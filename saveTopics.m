%%
% Load in the samples and show some topic-word and author-topic
% distributions
WO = textread( 'data/wordlist' , '%s' );
AN = textread('data/generic_authors', '%s');

%for c=1:NCHAINS
%    for s=1:NSAMPLES
%        filename = sprintf( 'out/at_chain%d_sample%d' , c , s );
%        fprintf( 'Loading sample #%d from chain #%d: filename=%s\n' , s , c , filename );
%       comm = sprintf( 'load ''%s''' , filename );
%        eval( comm );

        WPM{1} = WP; WPM{2} = AT;
        BETAM(1)=BETA; BETAM(2) = ALPHA;
        WOM{1}=WO; WOM{2}=AN;
        %%
        % Write the word topic and author topic distributions to a text file
        filename = sprintf('out/topics_chain_%d_%d', c, s);
        [ SM ] = WriteTopicsMult( WPM , BETAM , WOM , 10 , 100 , 1 , filename );

        fprintf( 'The first ten topic-word distributions:\n' );
        SM{1}(1:20)

        fprintf( 'The first ten author-topic distributions:\n' );
        SM{2}(1:10)
%    end
%end
