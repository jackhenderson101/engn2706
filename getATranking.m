function RANK  = getATranking( DOCUMENT )
%returns a ranking of all the author indices based on their similarity to
%the DOCUMENT specified. The first author index is the most similar
[~, DS] = importworddoccounts('data/wordcount');
SEEDS = [14,16,18,19];
dists = zeros(size(SEEDS,2), 3563);

for j = 1:size(SEEDS,2)
   filename = sprintf('out/at_chain_50_topics_seed%d.mat', SEEDS(j));
   dists(j,:) = generateDocDist(AUTHOR, DS, filename);
end

avgDists = mean(dists)';

%add document indicies to second row
avgDists(:,2) = 1:size(avgDists,1);
%sort by document distance
sorted = sortrows(avgDists,1);

RANK = sorted(:,2);
%this is used for the scatter plots
%add document rankings to third row
%sorted(:,3) = 1:size(avgDists,1);
%sorted = sortrows(sorted,2);
%RANK = sorted(:,3);

end

