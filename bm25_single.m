function SIM = bm25_single( WD, doc1, k, b )
%BM25_SINGLE Summary of this function goes here
%   Detailed explanation goes here

%% calculates the bm25 document similarity between two documents
if (nargin == 2)
	k=8.0;
	b=0.7;
end

% n = number of documents total
n = size(WD,2);

ndocs = sum(WD~=0,2);

% dl = wordcount of each document
dl = sum(WD,1);
avgdl = mean(dl);

SIM = zeros(1,n);
idf =log2(n./ndocs); % log2((n - ndocs+0.5)./(ndocs+0.5)); %% experimental %% 
%idf(idf<0) = 0;
idf(isinf(idf)) = 0;
bl_i = (1-b)+b*dl(doc1)/avgdl;

%fprintf('doc1=%d\n',doc1);

for doc2 = 1:size(WD,2)
    res = bm25(doc1,doc2,WD,k,b, dl, avgdl, idf, bl_i);
   SIM(1, doc2) = res;
end

end

